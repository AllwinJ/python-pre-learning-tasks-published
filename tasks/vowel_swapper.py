def vowel_swapper(string):
    # ==============
    # vowels = ['a','A','e','E','i','I','o','O','u','U']
    replacement = ['4','4','3','3','!','!','ooo','000','|_|','|_|']
    vowel_len = len(vowels)
    for i in range(vowel_len):
        str = str.replace(vowels[i], replacement[i])
    return str

print(vowel_swapper("aA eE iI oO uU"))
print(vowel_swapper("Hello World"))
print(vowel_swapper("Everything's Available"))

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
